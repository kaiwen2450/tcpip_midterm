#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>

int main(){
pid_t pid;
char buffer[4];
sigset_t s1;
if ((pid=fork())==-1)
exit(1);
else if(pid == 0){ //child process
sigfillset(&s1);
sigprocmask(SIG_BLOCK, &s1, NULL);
int infile=open("1171303283_1.dat",O_RDWR);
lseek(infile, -4, SEEK_END);
read(infile, &buffer, 4);
int outfile =open("fifo1", O_WRONLY| O_NONBLOCK);
write(outfile, &buffer, 4);
}
else{ //parent process
execl("./1171303283_2", "1171303283_2",NULL);
}
}
